type t = App of t * t | Var of int | Abs of t

(** Memory-preserving de-bruijn shift function *)
let rec shift offset depth = function
  | Var i when i > depth -> Var (i + offset)
  | App (t1, t2) as a ->
      let t1' = shift offset depth t1 and t2' = shift offset depth t2 in
      if t1 == t1' && t2 == t2' then a else App (t1', t2')
  | Abs t as a ->
      let t' = shift offset (depth + 1) t in
      if t == t' then a else Abs t'
  | t -> t

(** Memory-preserving de-bruijn substitution function *)
let rec substitute depth subterm = function
  | Var i when i = depth -> shift (depth - 1) 0 subterm
  | Var i when i > depth -> Var (i - 1)
  | App (t1, t2) as a ->
      let t1' = substitute depth subterm t1
      and t2' = substitute depth subterm t2 in
      if t1 == t1' && t2 == t2' then a else App (t1', t2')
  | Abs t as a ->
      let t' = substitute (depth + 1) subterm t in
      if t == t' then a else Abs t'
  | t -> t

(** Memory-preserving one-step beta reduction for De Bruijn terms.
    Uses the leftmost-outermost evaluation strategy. *)
let rec one_step_reduce = function
  | App (Abs t, u) -> substitute 1 u t
  | App (t1, t2) as a ->
      let t1' = one_step_reduce t1 in
      if t1 == t1' then
        let t2' = one_step_reduce t2 in
        if t2 == t2' then a else App (t1, t2')
      else App (t1', t2)
  | Abs t as a ->
      let t' = one_step_reduce t in
      if t == t' then a else Abs t'
  | t -> t

(** Reduction to the normal form of a term, if it exists. This can be costly and
    even non-terminating depending on the term. *)
let rec reduce t =
  let t' = one_step_reduce t in
  if t == t' then t else reduce t'
