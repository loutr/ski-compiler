open Calculus

let rec fold_int f init n = if n = 0 then init else fold_int f (f init) (n - 1)

(** builds the church encoding of the integer n *)
let from_int n =
  if n < 0 then raise @@ Invalid_argument "Church.from_int: n must be positive"
  else Abs (Abs (fold_int (fun t -> App (Var 2, t)) (Var 1) n))

(** Identifies the church encoding the term may correspond to. Returns [None]
    if the term is not a church encoding of an integer.
    Attention: Reduction is performed on the term. *)
let to_int t =
  let rec function_application_pattern = function
    | App (Var 2, t) -> Option.map (( + ) 1) (function_application_pattern t)
    | Var 1 -> Some 0
    | _ -> None
  in
  match reduce t with
  | Abs (Abs t) -> function_application_pattern t
  | _ -> None

let zero = from_int 0

(** successor of an integer *)
let succ_term = Abs (Abs (Abs (App (Var 2, App (App (Var 3, Var 2), Var 1)))))

let succ t = App (succ_term, t)

(** addition of integers *)
let plus_term =
  Abs
    (Abs (Abs (Abs (App (App (Var 4, Var 2), App (App (Var 3, Var 2), Var 1))))))

let plus m n = App (App (plus_term, m), n)

(** Predecessor function, with saturation at 0 *)
let pred_term  =
  (* \n. \f. \x. n (\g. \h. h (g f)) (\u. x) (\u. u) *)
  Abs
    (Abs
       (Abs
          (App
             ( App
                 ( App (Var 3, Abs (Abs (App (Var 1, App (Var 2, Var 4))))),
                   Abs (Var 2) ),
               Abs (Var 1) ))))

let pred t = App (pred_term, t)

(** substraction of integers *)
let minus_term = Abs (Abs (App (App (Var 1, pred_term), Var 2)))
let minus m n = App (App (minus_term, m), n)

(* boolean *)
let true_ = Abs (Abs (Var 2))
let false_ = Abs (Abs (Var 1))
let is_zero_term = Abs (App (App (Var 1, Abs false_), true_))
let is_zero t = App (is_zero_term, t)
let leq m n = is_zero (minus m n)
