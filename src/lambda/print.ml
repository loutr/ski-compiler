open Calculus

type source = Neutral | AppLeft | AppRight

let rec print_aux ~source = function
  | Var i -> print_int i
  | App (t1, t2) ->
      if source = AppRight then print_string "(";
      print_aux ~source:AppLeft t1;
      print_string " ";
      print_aux ~source:AppRight t2;
      if source = AppRight then print_string ")"
  | Abs t ->
      if source != Neutral then print_string "(";
      print_string "\\. ";
      print_aux ~source:Neutral t;
      if source != Neutral then print_string ")"

let print = print_aux ~source:Neutral
