type t = S | K | I | App of t * t | Var of int

(** Memory-preserving one-step reduction for SKI terms.
    Uses the leftmost-outermost evaluation strategy. *)
let rec one_step_reduce = function
  | (S | K | I | Var _) as m -> m
  | App (I, t) -> t
  | App (App (K, x), _) -> x
  | App (App (App (S, x), y), z) -> App (App (x, z), App (y, z))
  | App (t1, t2) as a ->
      let t1' = one_step_reduce t1 in
      if t1 == t1' then
        let t2' = one_step_reduce t2 in
        if t2 == t2' then a else App (t1, t2')
      else App (t1', t2)

(** Reduction to the normal form of a term, if it exists. This can be costly and
    even non-terminating depending on the term. *)
let rec reduce t =
  let t' = one_step_reduce t in
  if t == t' then t else reduce t'
