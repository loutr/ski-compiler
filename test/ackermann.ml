open Ski_compiler
open Lambda
open Calculus

(** Turing fixpoint combinator *)
let turing_fixpoint =
  let delta = Abs (Abs (App (Var 1, App (App (Var 2, Var 2), Var 1)))) in
  App (delta, delta)

let ackermann_body =
  let open Church in
  let case_m0 = succ (Var 1) in
  let case_n0 = App (App (Var 3, pred (Var 2)), succ zero) in
  let case_main =
    App (App (Var 3, pred (Var 2)), App (App (Var 3, Var 2), pred (Var 1)))
  in
  (* ackermann = Var 3 *)
  Abs
    (* m = Var 2 *)
    (Abs
       (* n = Var 1 *)
       (Abs
          (App
             ( App (is_zero (Var 2), case_m0),
               App (App (is_zero (Var 1), case_n0), case_main) ))))

let ackermann = App (turing_fixpoint, ackermann_body)

let ackermann_from_lambda m n =
  let tm = Church.from_int m and tn = Church.from_int n in
  let result = reduce (App (App (ackermann, tm), tn)) in
  Church.to_int result

let ackermann_from_ski_from_lambda m n =
  let tm = Church.from_int m and tn = Church.from_int n in
  let computation_ski =
    Ski.Compile.from_lambda (App (App (ackermann, tm), tn))
  in
  let result_ski = Ski.Calculus.reduce computation_ski in
  let result = Ski.Compile.to_lambda result_ski in
  Church.to_int result

let ackermann_from_ski_from_lambda' m n =
  let open Ski in
  let ackermann_ski : Ski.t =
    App
      ( Compile.from_lambda turing_fixpoint,
        Calculus.reduce (Compile.from_lambda ackermann_body) )
  in
  let ski_from_int n =
    Calculus.reduce (Compile.from_lambda (Church.from_int n))
  in
  let tn = ski_from_int n and tm = ski_from_int m in
  Printf.printf "[n] = \n";
  Ski.print tn;
  Printf.printf "\n\n[ackermann] = \n";
  Ski.print ackermann_ski;
  let computation_ski : Ski.t = App (App (ackermann_ski, tm), tn) in
  let result_ski = Ski.Calculus.reduce computation_ski in
  let result = Ski.Compile.to_lambda result_ski in
  Church.to_int result

let rec ackermann m n =
  if m = 0 then n + 1
  else if n = 0 then ackermann (m - 1) 1
  else ackermann (m - 1) (ackermann m (n - 1))

let () =
  ignore ackermann_from_ski_from_lambda;
  assert (Some (ackermann 2 3) = ackermann_from_lambda 2 3);
  assert (Some (ackermann 2 3) = ackermann_from_ski_from_lambda' 2 3)
