open Calculus

type source = AppLeft | AppRight

let rec print_aux ~source = function
  | S -> print_string "S"
  | K -> print_string "K"
  | I -> print_string "I"
  | Var i -> Printf.printf "[%d]" i
  | App (t1, t2) ->
      if source = AppRight then print_string "(";
      print_aux ~source:AppLeft t1;
      print_aux ~source:AppRight t2;
      if source = AppRight then print_string ")"

let print = print_aux ~source:AppLeft
