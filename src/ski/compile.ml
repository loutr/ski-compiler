open Calculus

let rec transform_abs = function
  | Var 1 -> I
  | (S | K | I) as m -> App (K, m)
  | Var i -> App (K, Var (i - 1))
  | App (m, n) -> App (App (S, transform_abs m), transform_abs n)

let rec from_lambda = function
  | (Var i : Lambda.t) -> Var i
  | App (u, v) -> App (from_lambda u, from_lambda v)
  | Abs t -> transform_abs (from_lambda t)

let rec to_lambda = function
  | I -> (Abs (Var 1) : Lambda.t)
  | K -> Abs (Abs (Var 2))
  | S -> Abs (Abs (Abs (App (App (Var 3, Var 1), App (Var 2, Var 1)))))
  | Var i -> Var i
  | App (m, n) -> App (to_lambda m, to_lambda n)
