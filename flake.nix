{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        oPkgs = pkgs.ocamlPackages;
      in
      rec {
        packages.default = oPkgs.buildDunePackage {
          pname = "ski_compiler";
          version = "0.1.0";

          duneVersion = "3";

          buildInputs = with oPkgs; [ ];

          src = ./.;

          meta = with pkgs.lib; {
            description = "";
            homepage = "";
            license = licenses.unlicense;
          };
        };

        devShells.default = pkgs.mkShell (with packages.default; {
          name = pname + "-dev";
          packages =
            buildInputs ++ nativeBuildInputs ++
            (with oPkgs; [ ocaml-lsp ocamlformat utop ]);
        });
      });
}
